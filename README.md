# Streaming List 2024

High quality movie, tv show, and anime streaming list for 2024.

## Intro

The following items are regularly checked to be safe to use, and still working. Please create an issue if you find one or more of the links have become unavailable or no longer follow the requirements below. Links can be in multiple list sections if they cover multiple categories. The link is expected to change over time as some websites change domains often.

## Requirements

All websites in the list must have or be the following:

- Easy to use website
- Can use the website without an account/for free
- Ads can be blocked with uBlock Origin
- Has video quality of 1080p or above
- Works on mobile
- No malware found with VirusTotal and URLVoid
- Can find a video and start watching in less than 1 minute

Note: Some scanners detect for streaming sites just for being streaming sites, this is to be ignored. The same for scanners which show detections because they couldn't find whois information.

## Movie Streaming

[Mp4Hydra.org](https://mp4hydra.org) - 1080p / Fast Loading / Multi server

[Fmovies](https://fmoviesz.to) - 1080p / Fast Loading / Multi server

[UpMovies](https://upmovies.to/) - 1080p / Fast Loading / Multi server

[sflix](https://sflix.to/) - 1080p / Fast Loading / Multi server

## TV Show Streaming

[Fmovies](https://fmoviesz.to) - 1080p / Fast Loading / Multi server

[UpMovies](https://upmovies.to/) - 1080p / Fast Loading / Multi server

[SFlix](https://sflix.to/) - 1080p / Fast Loading / Multi server

[BestSeries](https://bstsrs.one/) - 1080p / Fast Loading / Multi server

## Anime Streaming

[AniWave](https://aniwave.to/) - 1080p / Fast Loading / Multi server

[Anix](https://anix.to/) - 1080p / Fast Loading / Multi server

[KickAssAnime](https://kickassanime.mx) - 1080p / Fast Loading / Multi server

## Contributing

You can create pull requests to add new sites, as long as they follow the requirements above.
